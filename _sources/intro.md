# Welcome to Monte Carlo Techniques

<img src="images/logo.png" alt="Monte Carlo Techniques logo" width="400px">

## About this book
This interactive book collects the lecture notes for the course **NWI-NM042B Monte Carlo Techniques** taught in the Physics & Astronomy master program at Radboud University, Nijmegen, The Netherlands.

Please go ahead and browse the different chapters:

```{tableofcontents}
```

## Instructions 

Each chapter contains a variety of Python code snippets illustrating the material. In the preparation of this book the code has been executed and the output included statically in the HTML pages and the PDF version. But this does not mean that you cannot interact with the code. For each chapter there is a corresponding Jupyter notebook (in .ipynb format) that can be downloaded with the links at the top of the page. Note that not all content will render identically in the notebook, but the code blocks will be identical.

If you have access to the JupyterHub server of the Science faculty at Radboud University, then you may use the following NBGitPuller link to synchronize the whole book including Jupyter notebooks to your science account:

<https://jupyterhub22.science.ru.nl/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.science.ru.nl%2Ftbudd%2Fmonte-carlo-techniques&urlpath=tree%2Fmonte-carlo-techniques%2F_sources%2F&branch=main>

Otherwise you could, for instance, launch the book on MyBinder:

<https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.science.ru.nl%2Ftbudd%2Fmonte-carlo-techniques.git/main?filepath=_sources>

In either case you will find the relevant Jupyter notebooks in the directories `/monte-carlo-techniques/_sources/lectures/` and `/monte-carlo-techniques/_sources/exercises/`.

## About the author

[Timothy Budd](https://hef.ru.nl/~tbudd/) is an assistant professor at [High Energy Physics (HEP) department](https://www.ru.nl/highenergyphysics/) of the [Institute for Mathematics, Astrophysics and Particle Physics (IMAPP)](https://www.ru.nl/imapp/) at [Radboud University, Nijmegen](https://www.ru.nl).

:::{figure-md} timothybudd
<img src="images/tbudd.png" alt="Timothy Budd" width="120px">

Timothy Budd
:::
